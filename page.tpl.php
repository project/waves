<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body class="<?php print $body_classes; ?>">

  <div id="page"><div id="page-inner">

    <a name="top" id="navigation-top"></a>
    <div id="skip-to-nav"><a href="#navigation"><?php print t('Skip to Navigation'); ?></a></div>


    <div id="header"><div id="header-rightwave-background"><div id="header-inner" class="clear-block">


    <?php if (isset($secondary_links)) : ?>
      <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
    <?php endif; ?>

    <!-- right upper title -->
    <?php if ($site_name || $site_slogan): ?>
    <ul id="site-info">
      <?php if ($site_name):   ?><li id="site-name"  ><?php print $site_name;   ?></li><?php endif; ?>
      <?php if ($site_slogan): ?><li id="site-slogan"><?php print $site_slogan; ?></li><?php endif; ?>
    </ul>
    <?php endif; ?>


    <!-- logo -->
    <?php
    if ($logo) {
        print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
    }
    ?>

    </div></div></div> <!-- /#header-inner, /#header -->

    <!-- purple bar at the top of the page, beneath the headers -->
    <div id="topbar" class="region region-topbar">
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
    </div>

    <div id="main"><div id="main-inner" class="clear-block<?php if ($search_box || $primary_links || $secondary_links || $navbar) { print ' with-navbar'; } ?>">

      <div id="content"><div id="content-inner">

        <?php if ($mission): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>

        <?php if ($content_top): ?>
          <div id="content-top" class="region region-content_top">
            <?php print $content_top; ?>
          </div> <!-- /#content-top -->
        <?php endif; ?>

        <?php if ($breadcrumb or $title or $tabs or $help or $messages): ?>
          <div id="content-header">
            <?php print $breadcrumb; ?>
            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print $messages; ?>
            <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>
            <?php print $help; ?>
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

        <?php if ($content_bottom): ?>
          <div id="content-bottom" class="region region-content_bottom">
            <?php print $content_bottom; ?>
          </div> <!-- /#content-bottom -->
        <?php endif; ?>

      </div></div> <!-- /#content-inner, /#content -->


        <div id="sidebar-left"><div id="sidebar-left-topright-extender"><div id="sidebar-left-bottomright-extender"><div id="sidebar-left-inner" class="region region-left">

	  <h2><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home">
	  <?php if ($site_name): ?>
	    <?php print $site_name; ?>
	  <?php else: ?>
		  Home
	  <?php endif; ?>
	  </a></h2>

          <?php if ($left): ?>
              <?php print $left; ?>
          <?php endif; ?>

        </div></div></div></div> <!-- /#sidebar-left-inner, /#sidebar-left -->


        <div id="sidebar-right"><div id="sidebar-right-inner" class="region region-right">

          <?php if ($right): ?>
            <?php print $right; ?>
          <?php endif; ?>

        </div></div> <!-- /#sidebar-right-inner, /#sidebar-right -->

    </div></div> <!-- /#main-inner, /#main -->

    <div id="footer"><div id="footer-inner" class="region region-footer">

      <div id="footer-message"><?php print $footer_message; ?></div>

      <?php print $footer; ?>

    </div></div> <!-- /#footer-inner, /#footer -->

  </div></div> <!-- /#page-inner, /#page -->

  <?php if ($closure_region): ?>
    <div id="closure-blocks" class="region region-closure"><?php print $closure_region; ?></div>
  <?php endif; ?>

  <?php print $closure; ?>

</body>
</html>
